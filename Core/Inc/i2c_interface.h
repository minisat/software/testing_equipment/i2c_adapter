#ifndef _I2C_INTERFACE_H
#define _I2C_INTERFACE_H

#include "sg_stm32f1_i2c.h"

#define BUFFERSIZE				64

#define UART					USART2
#define START1					0xAB
#define START2					0xCD
#define COMMAND_WRITE			0x01
#define COMMAND_READ			0x02


/*
 * PROTOCOL:
 * REQUEST:
 *
 * START1
 * START2
 * SLAVEADDRESS
 * LENGTH
 * COMMAND
 * PAYLOAD
 *
 * RESPONSE:
 * STATUS (0 - ok, not 0 - I2C error
 * DATA (requested amount of data)
 */


#endif
