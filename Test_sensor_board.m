instrreset;

s = serial('COM7', 'BaudRate',115200);

fopen(s);

start1 = 171; %0xAB
start2 = 205; %0xCD
command_w = 1;
command_r = 2;
slave_addr = hex2dec('3C'); %lsm303dlh magn

length_w = 1;
length_r = 1;

payload = [12]; %read who am i

%% write test
clc;
a=1;
while a==1
	fwrite(s,uint8([start1 start2 slave_addr length_w command_w payload]));
	pause(0.1);
	len = s.BytesAvailable;
	if len>0
		res = fread(s, len);
		fprintf('status = %d\ndata: ',res(1));
		a = 2;
	else
		disp('no answer');
		pause(1);
	end
end
a = 2;
while a==2
	fwrite(s,uint8([start1 start2 slave_addr length_r command_r]));
	pause(0.1);
	len = s.BytesAvailable;
	if len>0
		res = fread(s, len);
		fprintf('status = %d\ndata: ',res(1));
		for i=2:length(res)
			fprintf('0x%c%c ', dec2hex(res(i)));
		end
		fprintf('\n');
		a = 3;
	else
		disp('no answer');
		pause(1);
	end
end
fprintf('\n');
%%
slave_addr = hex2dec('31'); %lsm303dlh magn
a=1;
while a==1
	fwrite(s,uint8([start1 start2 slave_addr length_w command_w payload]));
	pause(0.1);
	len = s.BytesAvailable;
	if len>0
		res = fread(s, len);
		fprintf('status = %d\ndata: ',res(1));
		a = 2;
	else
		disp('no answer');
		pause(1);
	end
end
a = 2;
while a==2
	fwrite(s,uint8([start1 start2 slave_addr length_r command_r]));
	pause(0.1);
	len = s.BytesAvailable;
	if len>0
		res = fread(s, len);
		fprintf('status = %d\ndata: ',res(1));
		for i=2:length(res)
			fprintf('0x%c%c ', dec2hex(res(i)));
		end
		fprintf('\n');
		a = 3;
	else
		disp('no answer');
		pause(1);
	end
end
fprintf('\n');
%%
slave_addr = hex2dec('EC'); %bmp280
payload = [hex2dec('D0')]; %read chip id

a=1;
while a==1
	fwrite(s,uint8([start1 start2 slave_addr length_w command_w payload]));
	pause(0.1);
	len = s.BytesAvailable;
	if len>0
		res = fread(s, len);
		fprintf('status = %d\ndata: ',res(1));
		a = 2;
	else
		disp('no answer');
		pause(1);
	end
end
a = 2;
while a==2
	fwrite(s,uint8([start1 start2 slave_addr length_r command_r]));
	pause(0.1);
	len = s.BytesAvailable;
	if len>0
		res = fread(s, len);
		fprintf('status = %d\ndata: ',res(1));
		for i=2:length(res)
			fprintf('0x%c%c ', dec2hex(res(i)));
		end
		fprintf('\n');
		a = 3;
	else
		disp('no answer');
		pause(1);
	end
end

fclose(s);